package com.example.demo.helpers;

import com.example.demo.model.Cat;
import com.example.demo.repositories.CatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;

@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {
    private boolean alreadySetup = true;
    private CatRepository catRepository;

    @Autowired
    public InitialDataLoader(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (alreadySetup) return;
        List<Cat> cats = new LinkedList<>();
        cats.add(new Cat("Мурзик","https://cdn2.thecatapi.com/images/6u9.jpg"));
        cats.add(new Cat("Пушок","https://cdn2.thecatapi.com/images/bjh.jpg"));
        cats.add(new Cat("Васька","https://cdn2.thecatapi.com/images/3p6.jpg"));
        cats.add(new Cat("Муся","https://cdn2.thecatapi.com/images/bcs.jpg"));
        cats.add(new Cat("Барсик","https://cdn2.thecatapi.com/images/a05.jpg"));
        cats.add(new Cat("Гарфилд","https://cdn2.thecatapi.com/images/7os.jpg"));
        cats.add(new Cat("Леопольд","https://cdn2.thecatapi.com/images/9rd.jpg"));
        cats.add(new Cat("Кнопка","https://cdn2.thecatapi.com/images/5bf.jpg"));
        cats.add(new Cat("Бонифаций","https://cdn2.thecatapi.com/images/a-__kzsPs.jpg"));
        cats.add(new Cat("Оливер","https://cdn2.thecatapi.com/images/bse.jpg"));
        cats.add(new Cat("Кексик","https://cdn2.thecatapi.com/images/MTk1ODY2Mw.jpg"));
        catRepository.saveAll(cats);

        alreadySetup = true;
    }
}
