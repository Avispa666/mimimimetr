package com.example.demo.services;

import com.example.demo.model.Cat;
import com.example.demo.model.VotingPair;

import java.util.List;

public interface VotingService {
    List<Cat> getTopTen();
    VotingPair getVotingPair(String voterId);
    void makeVote(Long catId, String voterId);
}
