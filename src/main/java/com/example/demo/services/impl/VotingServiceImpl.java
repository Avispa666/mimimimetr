package com.example.demo.services.impl;

import com.example.demo.model.Cat;
import com.example.demo.model.Vote;
import com.example.demo.model.VotingPair;
import com.example.demo.repositories.CatRepository;
import com.example.demo.repositories.VoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.services.VotingService;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class VotingServiceImpl implements VotingService {
    private static final Logger log = LoggerFactory.getLogger(VotingServiceImpl.class);
    private final VoteRepository voteRepository;
    private final CatRepository catRepository;

    @Autowired
    public VotingServiceImpl(VoteRepository voteRepository, CatRepository catRepository) {
        this.voteRepository = voteRepository;
        this.catRepository = catRepository;
    }

    @Override
    public List<Cat> getTopTen() {
        log.debug("Getting top list from DB");
//        return voteRepository.findAll().stream()
//                .collect(Collectors.groupingByConcurrent(Vote::getCat, Collectors.counting()))
//                .entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(10)
//                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        return voteRepository.getTopList().stream().limit(10).collect(Collectors.toList());
    }

    @Override
    public VotingPair getVotingPair(String voterId) {
        log.debug("Getting voting pair from DB for voter id=" + voterId);
        List<Cat> cats = catRepository.findAll();
        List<Cat> votedCats = voteRepository.findAll().stream().filter(vote -> Objects.equals(vote.getVoterId(), voterId))
                .map(Vote::getCat).collect(Collectors.toList());
//        List<Cat> votedCats = voteRepository.findByVoterId(voterId).stream().map(Vote::getCat).collect(Collectors.toList());
        cats.removeAll(votedCats);
        if (cats.isEmpty() || cats.size() < 2) {
            log.debug("No voting pairs left for voter id=" + voterId);
            return null;
        }
        return new VotingPair(cats.remove((int) (cats.size() * Math.random())),
                cats.remove((int) (cats.size() * Math.random())));
    }

    @Override
    public void makeVote(Long catId, String voterId) {
        log.debug("Voter id=" + voterId + " is making vote for cat id=" + catId);
        if(voteRepository.findByCatAndVoterId(new Cat(catId), voterId).isEmpty())
            voteRepository.saveAndFlush(new Vote(new Cat(catId), voterId));
    }
}
