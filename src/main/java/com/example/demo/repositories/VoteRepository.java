package com.example.demo.repositories;

import com.example.demo.model.Cat;
import com.example.demo.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VoteRepository extends JpaRepository<Vote, Long> {

    @Query(value = "SELECT new Cat(c.id, c.name, c.picUrl) FROM Vote v INNER JOIN v.cat AS c GROUP BY c.id ORDER BY COUNT(v.voterId) DESC")
    List<Cat> getTopList();

    List<Vote> findByCatAndVoterId(Cat cat, String voterId);
    List<Vote> findByVoterId(String voterId);

}
