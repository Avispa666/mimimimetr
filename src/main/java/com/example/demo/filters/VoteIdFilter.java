package com.example.demo.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class VoteIdFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(VoteIdFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Cookie[] cookies = request.getCookies();
        boolean isPresent = false;
        if (cookies != null) {
            for (Cookie c : cookies) {
                if("voter_id".equals(c.getName())) {
                    isPresent = true;
                    log.debug("voter_id is present");
                    break;
                }
            }
        }
        if(!isPresent) {
            Cookie c = new Cookie("voter_id", UUID.randomUUID().toString());
//            c.setMaxAge(7 * 24 * 60 * 60);
            c.setHttpOnly(true);
            log.debug("created voter_id = " + c.getValue());
            response.addCookie(c);
            response.sendRedirect("/");
            return;
        }
        filterChain.doFilter(request, response);
    }
}
