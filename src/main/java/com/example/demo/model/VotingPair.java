package com.example.demo.model;

import java.util.Objects;

public class VotingPair {
    private Cat first;
    private Cat second;

    public VotingPair(Cat first, Cat second) {
        this.first = first;
        this.second = second;
    }

    public Cat getFirst() {
        return first;
    }

    public Cat getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VotingPair that = (VotingPair) o;
        return first.equals(that.first) &&
                second.equals(that.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }
}
