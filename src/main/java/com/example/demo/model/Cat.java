package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Cat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String picUrl;

    public Cat(Long id) {
        this.id = id;
    }

    public Cat(String name, String picUrl) {
        this.name = name;
        this.picUrl = picUrl;
    }

    public String getName() {
        return name;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        if (name == null && picUrl == null) return Objects.equals(id, cat.id);
        return Objects.equals(name, cat.name) &&
                Objects.equals(picUrl, cat.picUrl) ||
                 Objects.equals(id, cat.id);
    }

//    @Override
//    public int hashCode() {
//        return Objects.hash(name, picUrl);
//    }
}
