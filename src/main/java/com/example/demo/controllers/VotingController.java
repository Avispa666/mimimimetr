package com.example.demo.controllers;

import com.example.demo.model.VotingPair;
import com.example.demo.services.VotingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
public class VotingController {
    private static final Logger log = LoggerFactory.getLogger(VotingController.class);
    private final VotingService votingService;

    @Autowired
    public VotingController(VotingService votingService) {
        this.votingService = votingService;
    }

    @GetMapping("/")
    public String getVotingPage(Model model, @CookieValue(name = "voter_id") String voterId,
                                @RequestParam(name = "cat_id", required = false) String catId) {
        if (catId != null) {
            votingService.makeVote(Long.parseLong(catId), voterId);
        }
        VotingPair vp = votingService.getVotingPair(voterId);
        if(vp != null) {
            log.debug(vp.getFirst().getName() + " " + vp.getFirst().getPicUrl());
            model.addAttribute("first", vp.getFirst());
            model.addAttribute("second", vp.getSecond());
            return "voting";
        } else { //all cats are voted
            return "redirect:/top";
        }
    }
}
