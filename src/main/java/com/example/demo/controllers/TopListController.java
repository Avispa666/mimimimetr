package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import com.example.demo.services.VotingService;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TopListController {
    private VotingService votingService;

    @Autowired
    public TopListController(VotingService votingService) {
        this.votingService = votingService;
    }

    @GetMapping("/top")
    public String getTopList(Model model, @CookieValue(name = "voter_id") String voterId) {
        model.addAttribute("topList", votingService.getTopTen());
        return "top";
    }
}
