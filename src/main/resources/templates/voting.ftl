<#import "/spring.ftl" as spring/>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Voting page</title>
        <style>
            img {
                display: inline;
                width: 400px;
                height: 400px;
            }
            #title {
                text-align: center;
                margin: 10px;
            }
            #cat-block {
                text-align: center;
                display: table;
                margin: 0 auto;
            }
            .cat {
                display: table-cell;
                padding: 10px;
            }
            .cat-link {
                display: block;
            }
            .name {
                text-align: center;
            }
            p {
                font-size: large;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div id="title">
            <h1>Мимимиметр</h1>
            <p>Кто из них мимимишнее? Кликни по фото!</p>
        </div>
        <div id="cat-block">
            <div id="left" class="cat">
                <a href="<@spring.url '/?cat_id=${first.id}'/>" class="cat-link">
                    <img src="${first.picUrl}">
                    <div class="name">
                        ${first.name}
                    </div>

                </a>
            </div>
            <div id="right" class="cat">
                <a href="<@spring.url '/?cat_id=${second.id}'/>" class="cat-link">
                    <img src="${second.picUrl}">
                    <div class="name">
                        ${second.name}
                    </div>
                </a>
            </div>
        </div>

    </body>
</html>