<#import "/spring.ftl" as spring/>
<!DOCTYPE HTML>
<html>
<head>
    <title>Top 10 page</title>
    <style>
        img {
            /*display: inline;*/
            width: 100px;
            height: 100px;
        }
    </style>
</head>
<body>
<h1>Top 10 page</h1>
<ol>
<#--    <#list topList as k, v>-->
    <#list topList as k>
        <li>
            <img src="${k.picUrl}"/> ${k.name}
        </li>
    </#list>
</ol>

</body>
</html>