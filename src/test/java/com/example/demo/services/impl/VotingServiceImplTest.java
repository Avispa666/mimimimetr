package com.example.demo.services.impl;

import com.example.demo.model.Cat;
import com.example.demo.model.Vote;
import com.example.demo.model.VotingPair;
import com.example.demo.repositories.CatRepository;
import com.example.demo.repositories.VoteRepository;
import com.example.demo.services.VotingService;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class VotingServiceImplTest {
    private static VoteRepository voteRepository;
    private static CatRepository catRepository;
    private static VotingService votingService;

    @BeforeEach
    void setUp() {
        catRepository = Mockito.mock(CatRepository.class);
        voteRepository = Mockito.mock(VoteRepository.class);
        votingService = new VotingServiceImpl(voteRepository, catRepository);
    }

    @Test
    void getTopTen() {
        Mockito.when(voteRepository.getTopList()).thenReturn(Arrays.asList(new Cat[20]));
        List<Cat> topTen = votingService.getTopTen();
        assertEquals(10, topTen.size());
        //query logic is in sql so we can't test it here
    }

    @Test
    void getVotingPair() {
        List<Cat> all = new LinkedList<>();
        all.add(new Cat((long) 1));
        all.add(new Cat((long) 2));
        all.add(new Cat((long) 3));
        all.add(new Cat((long) 4));
        Mockito.when(catRepository.findAll()).thenReturn(new LinkedList<>(all));

        List<Vote> voted = new LinkedList<>();
        voted.add(new Vote(new Cat((long) 1), "1"));
        Mockito.when(voteRepository.findByVoterId("1")).thenReturn(voted);

        VotingPair vp = votingService.getVotingPair("1");
        assertNotNull(vp.getFirst());
        assertNotNull(vp.getSecond());
        assertNotEquals(vp.getFirst(), vp.getSecond());

        Mockito.when(catRepository.findAll()).thenReturn(new LinkedList<>(all));

        //vote for someone
        voted.add(new Vote(new Cat((long) 2), "1"));
        VotingPair vp2 = votingService.getVotingPair("1");
        assertNotNull(vp2.getFirst());
        assertNotNull(vp2.getSecond());
        assertNotEquals(vp2.getFirst(), vp2.getSecond());
        assertTrue(vp2.getFirst().getId() == 3 || vp2.getSecond().getId() == 3);
        assertTrue(vp2.getFirst().getId() == 4 || vp2.getSecond().getId() == 4);

        Mockito.when(catRepository.findAll()).thenReturn(new LinkedList<>(all));

        //vote for someone again
        voted.add(new Vote(new Cat((long) 3), "1"));
        VotingPair vp3 = votingService.getVotingPair("1");
        assertNull(vp3);
    }

    @Test
    void makeVote() {
        Mockito.when(voteRepository.findByCatAndVoterId(new Cat((long) 1), "1")).thenReturn(new LinkedList<>());
        votingService.makeVote((long) 1, "1");
        Mockito.verify(voteRepository, Mockito.times(1)).findByCatAndVoterId(new Cat((long)1), "1");
        Mockito.verify(voteRepository, Mockito.times(1)).saveAndFlush(new Vote(new Cat((long) 1), "1"));

        List<Vote> v = new LinkedList<>();
        v.add(new Vote(new Cat((long)1), ""));
        Mockito.when(voteRepository.findByCatAndVoterId(new Cat((long)1),"1")).thenReturn(v);
        votingService.makeVote((long) 1, "1");
        Mockito.verify(voteRepository, Mockito.times(2)).findByCatAndVoterId(new Cat((long)1), "1");
        Mockito.verify(voteRepository, Mockito.times(1)).saveAndFlush(Mockito.any());
        Mockito.verifyNoMoreInteractions(voteRepository);
    }
}